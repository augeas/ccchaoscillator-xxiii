# ccchaoscillator-xxiii

Chaotic attractor noises, by way of the [4th Order Runge Kutta method](https://en.wikipedia.org/wiki/Runge%E2%80%93Kutta_methods).
(Don't use Euler's method. You're better than that.)

Left and right on the left button toggles CV out on the corresponding socket.
Press the left button to toggle drums when the trace crosses x = 0.

ToDo:
* Attractors other than [Lorenz](https://en.wikipedia.org/wiki/Lorenz_system).
* Parameter editing.

[See also.](http://augeas.github.io/Chaoscillator/)
