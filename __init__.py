
from math import sqrt
from random import random

import badgelink
import bl00mbox
import leds
import machine
from st3m.application import Application, ApplicationContext
from st3m.input import InputController
import st3m.run

class RK4(object):

    def __init__(self, h=0.01):
        self.h = h
        self.hh = h / 2

        self.ranges = [mx - mi for mi, mx in self.limits]

    def diff(self, vec, var, delta):
        return [v  if i != var else v + delta
            for i, v in enumerate(vec)]

    def deltas(self, vec, kvec, mult):
        return zip(
            self.partials,
            (self.diff(vec, i, mult * k) for i, k in enumerate(kvec))
        )

    def step(self, vec, t):
        hdt = t + self.hh
        dt = t + self.h

        K1 = [f(t, vec) for f in self.partials]
        K2 = [f(hdt, delta) for f, delta in self.deltas(vec, K1, self.hh)]
        K3 = [f(hdt, delta) for f, delta in self.deltas(vec, K2, self.hh)]
        K4 = [f(dt, delta) for f, delta in self.deltas(vec, K3, self.h)]

        return [
            v + self.h / 6 * (k1 + 2 * k2 +  2 * k3 + k4)
            for v, k1, k2, k3, k4 in zip(vec, K1, K2, K3, K4)
        ]

    def __iter__(self):
        vec = [0.01 * random() for _ in self.partials]
        while True:
            vec = self.step(vec, 0)
            yield vec

    def trace(self):
        all_points = self.__iter__()
        last_point = all_points.__next__()
        this_point = all_points.__next__()
        while True:
            scaled = [(p - lim[0]) / rng for p, lim, rng
                in zip(this_point, self.limits, self.ranges)]

            triggers = [nxt < 0 != prv < 0
                for nxt, prv in zip(this_point, last_point)]

            yield scaled, triggers

            last_point = this_point
            this_point = all_points.__next__()

class Lorenz(RK4):

    def __init__(self):
        self.rho = 28.0
        self.sigma = 10.0
        self.beta = 8.0 / 3

        self.partials = (self.dxdt, self.dydt, self.dzdt)

        self.limits = ((-20.0, 25.0), (-40.0, 40.0), (0, 60))

        super(Lorenz, self).__init__(0.02)

    def dxdt(self, t, vec):
        x, y, z = vec
        return self.sigma * (y - x)

    def dydt(self, t, vec):
        x, y, z = vec
        return x * (self.rho - z) - y

    def dzdt(self, t, vec):
        x, y, z = vec
        return x * y - self.beta * z

class Chua(RK4):

    def __init__(self):
        self.alpha = 15.6
        self.beta = 25.0

        self.m1 = -8 / 7
        self.m0 = -5 / 7
        self.m0m1 = 0.5 * (self.m0 - self.m1)

        self.partials = (self.dxdt, self.dydt, self.dzdt)

        self.limits = ((-3, 3), (-0.5, 0.5), (-5, 5))

        super(Chua, self).__init__(0.02)

    def chew_a_diode(self, h):
        return self.m1 * h + self.m0m1 * (abs(h + 1) - abs(h - 1))

    def dxdt(self, t, vec):
        x, y, z = vec
        return self.alpha * (y - x - self.chew_a_diode(x))

    def dydt(self, t, vec):
        x, y, z = vec
        return x - y + z

    def dzdt(self, t, vec):
        x, y, z = vec
        return -self.beta * y


class Chaoscillator(Application):
    def __init__(self, app_ctx):
        super().__init__(app_ctx)

        self.input = InputController()

        self.attractor = Lorenz()
        self.trace = self.attractor.trace()
        self.pos, _ = self.trace.__next__()

        self.trail = 128
        self.points = [self.point(self.pos[0], self.pos[2])]
        self.last_point = 0

        self.last_led = 0

        self.twang_debounce = False
        self.twang_on = False

        self.left_cv = False
        self.right_cv = False

        self.left_tip = None
        self.left_ring = None

        self.right_tip = None
        self.right_ring = None

        self.chan = bl00mbox.Channel('chaosxxiii')
        self.init_audio()

    def init_audio(self):
        self.chan.volume = 8192
        self.mix = self.chan.new(bl00mbox.plugins.mixer)

        self.osc1 = self.chan.new(bl00mbox.plugins.osc_fm)
        self.osc2 = self.chan.new(bl00mbox.plugins.osc_fm)

        self.lp1 = self.chan.new(bl00mbox.plugins.lowpass)
        self.lp2 = self.chan.new(bl00mbox.plugins.lowpass)

        self.osc1.signals.output = self.lp1.signals.input
        self.osc2.signals.output = self.lp2.signals.input

        self.lp1.signals.output = self.mix.signals.input0
        self.lp2.signals.output = self.mix.signals.input1

        self.twanger = self.chan.new(bl00mbox.patches.karplus_strong)
        self.twanger.signals.output = self.mix.signals.input2
        self.twanger.decay = 125

        self.mix.signals.output = self.chan.mixer

    def point(self, x, y):
        return (-100 + 200 * x, -100 + 200 * (1-y))

    def rgb_led(self, point):
        return point

    def synth_update(self, pos, triggers):
        self.osc1.signals.pitch.freq = 100 + 400 * pos[0]
        self.osc1.signals.lin_fm.value = 5000 * pos[1]
        self.lp1.signals.freq = 100 + 500 * pos[2]

        self.osc2.signals.pitch.freq = 100 + 400 * pos[1]
        self.osc2.signals.lin_fm.value = 5000 * pos[2]
        self.lp2.signals.freq = 100 + 500 * pos[0]

        if triggers[0] and not self.twang_debounce:
            if self.twang_on:
                self.twanger.signals.trigger.start()
            self.twang_debounce = True
        if not triggers[0]:
            self.twang_debounce = False

    def fix_pwm(self, p):
        pwm = int(1023 * p)
        if pwm < 0:
            return 0
        elif pwm > 1023:
            return 1023
        else:
            return pwm

    def draw(self, ctx):
        ctx.rgb(0, 0, 0).rectangle(-120, -120, 240, 240).fill()

        if not (self.left_cv or self.right_cv):
            ctx.rgb(0, 255, 0)
        else:
            ctx.rgb(255 * self.left_cv, 0, 255 * self.right_cv)

        for point in self.points   :
            h, k = point
            ctx.rectangle(h, k, 1, 1).fill()

    def get_pwm(self, right=True):
        if right:
            jack = badgelink.right
        else:
            jack = badgelink.left

        jack.enable()

        tip_pin = jack.tip.pin
        ring_pin = jack.ring.pin

        return machine.PWM(tip_pin), machine.PWM(ring_pin)

    def drop_pwm(self, right=True):
        if right:
            jack = badgelink.right
        else:
            jack = badgelink.left

        jack.tip.disable()
        jack.ring.disable()
        jack.disable()

    def think(self, ins: InputState, delta_ms: int):
        self.input.think(ins, delta_ms)

        x, y, z = self.pos
        self.pos, triggers = self.trace.__next__()
        x, y, z = self.pos

        new_point = self.point(self.pos[0], self.pos[2])
        if len(self.points) < self.trail:
            self.points.append(new_point)
        else:
            self.points[self.last_point] = new_point
            self.last_point = (self.last_point + 1) % self.trail

        re, gr, bl = self.rgb_led(self.pos)
        leds.set_rgb(self.last_led, re, gr, bl)
        self.last_led = (self.last_led + 1) % 40
        leds.update()

        left_button = ins.buttons.app

        if self.input.buttons.app.right.pressed:
            self.right_debounce = True
            self.right_cv = not self.right_cv
            if self.right_cv:
                self.right_tip, self.right_ring = self.get_pwm(True)
            else:
                self.drop_pwm(True)
                self.right_tip = None
                self.right_ring = None

        if self.input.buttons.app.left.pressed:
            self.right_debounce = True
            self.left_cv = not self.left_cv
            if self.left_cv:
                self.left_tip, self.left_ring = self.get_pwm(False)
            else:
                self.drop_pwm(False)
                self.left_tip = None
                self.left_ring = None

        if self.right_cv:
            self.right_ring.duty(self.fix_pwm(x))
            self.right_tip.duty(self.fix_pwm(z))

        if self.left_cv:
            self.left_ring.duty(self.fix_pwm(y))
            self.left_tip.duty(self.fix_pwm(z))
        else:
            self.synth_update(self.pos, triggers)

        if self.input.buttons.app.middle.pressed:
            self.twang_on = not self.twang_on
            self.twang_toggle_debounce = True

        super().think(ins, delta_ms)

    def on_exit(self):
        self.chan.free = True

if __name__ == '__main__':
    st3m.run.run_view(Chaoscillator(ApplicationContext()))
